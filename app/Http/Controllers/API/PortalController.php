<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

class PortalController extends Controller
{
    public function createAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "username" => 'required|unique:users|max:16|min:3',
            "email" => 'required|unique:users|email',
            "password" => 'required|min:6|same:confirmpassword'
        ]);
        if($validator->passes()) {
            $user = new User();
            $token = md5($request->input("username") . Str::uuid());
            $user->username = $request->input("username");
            $user->password = Hash::make($request->input("password"));
            $user->email = $request->input("email");
            $user->session_token = $token;
            $user->session_token_expires = Carbon::now()->add(7, "day");
            $user->save();

            return response()->json(["success" => true, "token" => $token]);
        }else{
            return response()->json(["success" => false, "errors" => $validator->errors()->all()]);
        }
    }

    public static function getUserByToken($token)
    {
        $tokenCheck = DB::table("users")->where("session_token", $token);
        if($tokenCheck->get()->count() == 1) return $tokenCheck->first();
        else return false;
    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            $token = md5($request->input("username") . Str::uuid());
            $user = User::where("username", $request->input("username"))->first();
            $user->session_token = $token;
            $user->save();
            return response()->json(["success" => true, "token" => $token]);
        }else{
            return response()->json(["success" => false]);
        }
    }

    public function validToken(Request $request)
    {
        $tokenCheck = DB::table("users")->where("session_token", $request->input("token"));
        if($tokenCheck->get()->count() == 1) return response()->json(["valid" => true]);
        else return response()->json(["valid" => false]);
    }
}
