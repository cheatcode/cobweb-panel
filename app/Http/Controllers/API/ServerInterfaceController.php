<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerInterfaceController extends Controller
{
    public function setToken(Request $request)
    {
        return response()->json(["token" => $request->input("token")]);
    }
}
