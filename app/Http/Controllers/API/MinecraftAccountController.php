<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\MojangAPI;

class MinecraftAccountController extends Controller
{
    public function listAccounts(Request $request)
    {
        $user = PortalController::getUserByToken($request->header("X-Cobweb-Token"));
        if($user == false) {
            return response()->json(["success" => false, "message" => "Not Authorised"])->setStatusCode(401);
        }
        $accounts = DB::table("minecraft_accounts")->select("username", "uuid", "created_at", "confirmed", "verifyCode", "updated_at")->where("owner", $user->id);
        return response()->json(["success" => true, "accounts" => $accounts->get()]);
    }

    public function addAccount(Request $request)
    {
        $user = PortalController::getUserByToken($request->header("X-Cobweb-Token"));
        if($user == false) {
            return response()->json(["success" => false, "message" => "Not Authorised"])->setStatusCode(401);
        }
        $username = $request->input("username");
        $validator = Validator::make($request->all(), [
            "username" => 'required|min:3|max:16|regex:/^[a-zA-Z0-9_]*$/'
        ]);
        if($validator->fails()) {
            return response()->json(["success" => false, "error" => $validator->errors()->first()]);
        }

        $account = MojangAPI::getProfile($username);
        if(!is_array($account)) {
            return response()->json(["success" => false, "error" => "That Minecraft account does not exist."]);
        }

        if(DB::table("minecraft_accounts")->where("uuid", $account["id"])->get()->count() > 0) {
            return response()->json(["success" => false, "error" => "Someone else has already linked that account."]);
        }
        if(DB::table("minecraft_accounts")->where("confirmed", false)->get()->count() > 0) {
            return response()->json(["success" => false, "error" => "You need to verify all other accounts before you can add a new one."]);
        }

        $code = substr(md5(rand(1, 10) . microtime()), 0, 8);

        DB::table("minecraft_accounts")->insert([
            "owner" => $user->id,
            "uuid" => $account["id"],
            "username" => $account["name"],
            "confirmed" => false,
            "primary" => false,
            "verifyCode" => $code,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);

        return response()->json(["success" => true, "profile" => $account, "code" => $code, "timestamp" => Carbon::now()]);
    }
}
