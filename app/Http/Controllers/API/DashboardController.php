<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function base() {
        // Ensure user is logged in

        // Create array
        $response = [];

        // Populate w/ user information
        $user = auth('api')->user();
        if(is_null($user)) return response()->json(["error" => true, "code" => 401, "message" => "Missing API Token."], 401);

        $response["user"]["username"] = $user->username;
        $response["user"]["uuid"] = $user->uuid;
        $response["user"]["type"] = $user->account_type;
        $response["user"]["email"] = $user->email;
        $response["user"]["created_at"] = strtotime($user->created_at);

        $response["servers"] = [];

        $response["news"] = ["This application is in development mode."];

        return response()->json($response, 200);
    }

    public function checkLink(Request $request) {
        // Create array
        $response = [];

        // Populate w/ user information
        $user = auth('api')->user();

        if(strlen($user->uuid) == 0) {
            $response["success"] = false;
            $response["message"] = "Please run the link command in-game to continue.";
        }else{
            $response["success"] = true;
            $response["uuid"] = $user->uuid;
        }
        return response()->json($response);
    }

}
