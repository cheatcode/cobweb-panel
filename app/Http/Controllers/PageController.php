<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function handleRoute(Request $request, $route = "") {
        return view("themes.cobweb.app");
    }
}
