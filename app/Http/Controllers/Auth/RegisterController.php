<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cloud/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new \App\User();
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->api_token = Str::random(60);
        $user->uuid = "";
        $user->save();

    }

    public function cobwebRegister(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required|profanity|unique:users|min:4|max:16',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:64',
            'confirmpassword' => 'required|same:password'
        ], ["confirmpassword.same" => "The passwords must match.", "confirmpassword.required" => "Please repeat your password.", "profanity" => "Please use an appropriate name."]);

        if($validator->fails()) {
            return response()->json(["success" => false, "message" => $validator->errors()->first()]);
        }

        $this->create([
            "username" => $request->input("username"),
            "email" => $request->input("email"),
            "password" => $request->input("password")
        ]);

        return response()->json(["success" => true, "message" => "Created Account."]);
    }
}
