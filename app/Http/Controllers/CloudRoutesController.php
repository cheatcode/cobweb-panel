<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CloudRoutesController extends Controller
{
    public function login() {
        if(Auth::check()) {
            return redirect('/cloud/dashboard');
        }else{
            return view("themes.cobweb.cloud.login");
        }
    }
    public function register() {
        if(Auth::check()) {
            return redirect('/cloud/dashboard');
        }else{
            return view("themes.cobweb.cloud.register");
        }
    }
}
