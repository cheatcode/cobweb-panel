<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function baseDashboard() {
        if(Auth::check()) {
            return view("themes.cobweb.cloud.dashboard");
        } else {
            return redirect('/cloud/login');
        }
    }

    public function setSession(Request $request) {
        $user = User::where("api_token", $request->get("token"))->first();
        if($user == null) {
            return redirect("/cloud/login");
        }else{
            Auth::login($user, TRUE);
            return redirect("/cloud/dashboard");
        }
    }
}
