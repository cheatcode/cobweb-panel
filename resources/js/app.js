import Vue from 'vue';
import VueRouter from 'vue-router';
import AOS from 'aos';
import 'aos/dist/aos.css'
import Routes from"./routes";

Vue.use(VueRouter);

/**
 * Let's create ourselves a vue-router instance.
 */


const Welcome = require("./themes/cobweb/Welcome").default;
const Portal = require("./themes/cobweb/auth/Portal").default;
const Dashboard = require("./themes/cobweb/Dashboard").default;
const AccountLink = require("./themes/cobweb/profile/AccountLink").default;
const Error404 = require("./themes/cobweb/errors/404").default;


const router = new VueRouter({
    routes: [{
                path: '/',
                component: Welcome
            },{
                path: '/auth/:type',
                component: Portal
            },
            {
                path: '/dashboard',
                component: Dashboard
            },
            {
                path: '/profile/accounts',
                component: AccountLink
            },

            {
                path: '*',
                component: Error404
            }

            ],
    mode: 'history'
})

if(localStorage.getItem("token") !== null) {
    axios.defaults.headers.common = {
        "X-Cobweb-Token": localStorage.getItem("token")
    };
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    created () {
        AOS.init()
    },
    data: function(){
        return {
            loaded: false, // Used by various services to communicate across VueJS components
            alert: {
                show: false,
                type: "info",
                content: "Failed to load alert."
            }
        }
    },
    template: "<router-view />",
    router: router
});
