{{--
 This is the base file for all routes using Cobweb.
 To create/modify routes, open /resources/js/routes.js
 --}}
<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title></title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="{{mix("/css/vendor.min.css")}}">
        <link rel="stylesheet" href="{{mix("/css/cobweb.min.css")}}">
    </head>
    <body>
        <div id="app"></div>
        <script src="{{mix("/js/vendor.min.js")}}"></script>
        <script src="{{mix("/js/cobweb.min.js")}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0/js/all.min.js"></script>
    </body>
</html>
