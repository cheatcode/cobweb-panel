<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Pass all routes to Vue, the ->where allows the base URL at / to also fall under this rule.
Route::get("/{route}", 'PageController@handleRoute')->where('route', '.*')->middleware('cors');
