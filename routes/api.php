<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(["prefix" => "v1"], function() {
    Route::group(["prefix" => "portal"], function() {
        Route::post("createAccount", "API\PortalController@createAccount");
        Route::post("login", "API\PortalController@login");
        Route::post("validToken", "API\PortalController@validToken");
    });
    Route::group(["prefix" => "profile"], function() {
        Route::group(["prefix" => "accounts"], function() {
            Route::get("list", "API\MinecraftAccountController@listAccounts");
            Route::post("add", "API\MinecraftAccountController@addAccount");
        });
    });
    Route::group(["prefix" => "server"], function() {
        Route::post("setToken", "API\ServerInterfaceController@setToken");
    });
});
