# Cobweb
## Welcome
Welcome to the **Cobweb Panel** source code!

Please note that this is only the main game panel, and *not* the Spigot plugin. The plugin can be found [here](https://gitlab.com/cheatcode/cobweb-plugin).

## What is Cobweb?
Cobweb is a Minecraft server administration utility which runs standalone from your main control panel (whether it be Multicraft, Pterodactyl, SSH/screens or other tools).
It is installed through a plugin running on your server and uses its own internal REST API to handle communication between the server and the panel.

## System Requirements
**Spigot Plugin**
- Spigot 1.8 or higher
- An external facing IP (see [here](https://gitlab.com/cheatcode/cobweb-panel/-/wikis/Installing-on-Pterodactyl) for how to do this on Pterodactyl)
- A spare port on your server

**Panel**
-   PHP >= 7.1.3 with the following extensions
	- BCMath
	- Ctype
	- JSON
	- MBString
	- OpenSSL
	- PDO
	- Tokenizer
	- XML
- NodeJS & NPM
	- Yarn is optional but preferred
- Webserver of your Choice (we suggest Nginx)
- Composer
- MariaDB or MySQL
- Optionally
	- Redis for Caching
## Installation
### MySQL

*If you do not have a MySQL database/account created, you can do the following. Remember to replace [PASSWORD] with a strong password.*
```
# If you have a MySQL root password, add -p to the end of this command (so mysql -uroot -p)
$ mysql -uroot
> CREATE DATABASE cobweb;
> CREATE USER 'cobweb'@'localhost' IDENTIFIED BY '[PASSWORD]';
> GRANT ALL PRIVILEGES ON cobweb.* TO 'cobweb'@'localhost';
> exit
```

### Panel
To install **Cobweb Panel** on your local machine, run the following:
```
$ mkdir -p /var/www/cobweb
$ git clone git@gitlab.com:cheatcode/cobweb .
$ composer install
$ php artisan cobweb:install
$ cp .env.example .env
$ chmod -r 0644 ./
```
You will then be presented with an installation wizard (in terminal).
```
---------------------------------
Welcome to the Cobweb Installer.
---------------------------------
```

`[?] Is this application for use in production (yes): `
If the application is in production, we will minify JavaScript/CSS and disable error messages (sans an error screen).
Example: `yes`

`[?] What is the URL of this Cobweb Panel? :`
This is the URL Cobweb will attempt to use for server communication, canonical URLs and if anything goes wrong.
Example: `https://cobweb.cheatcodemedia.net`

`[?] What is the URL of this Cobweb Panel? (http://localhost):`
This is the URL Cobweb will attempt to use for server communication, canonical URLs and if anything goes wrong.
Example: `https://cobweb.cheatcodemedia.net`

`[?] What is the MySQL hostname? (localhost):`
This is the hostname of your MySQL server
Example: `mysql.minecrafthost.net`

`[?] What is the MySQL port? (3306):`
This is the port of your MySQL server
3306 is the default MySQL port. If you haven't been given a different port, use this.

`[?] What is the MySQL username? (cobweb):`
This is the username of the MySQL account with access to Cobweb's database.

`[?] What is the MySQL password? :`
This is the password of the MySQL account with access to Cobweb's database

`[?] What is the MySQL database name? (cobweb):`
This is the Cobweb MySQL database. It should be newly created (see `MySQL` setup for more information on how to do this)

`[?] Is all the information correct? (yes):`
Make sure everything you've entered is 100% correct. Answer `yes` or `no` to this and it will either abort or start the installation.

```
Installing Cobweb...
-> Generating Application Key
-> Inserting MySQL Details
-> Testing MySQL Connection
-> Running Migrations
-> Building Frontend (this may take a minute)
-> Flushing Cache and Saving Configuration...
--------------------------------------------
CONGRATULATIONS
Cobweb is now installed on this server.
You may need to configure it on your webserver
before it can be accessed. To generate a
configuration file for Nginx, type
php artisan cobweb:webserver nginx
--------------------------------------------
```
And you're done!
