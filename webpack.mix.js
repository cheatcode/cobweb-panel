const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/vendor.js', 'public/js/vendor.min.js')
    .js('resources/js/app.js', 'public/js/cobweb.min.js')
    .version()
    .styles(['resources/css/argon.css'], 'public/css/vendor.min.css')
    .sass('resources/css/app.scss', 'public/css/cobweb.min.css')
;

mix.webpackConfig({ node: { fs: 'empty' }})
