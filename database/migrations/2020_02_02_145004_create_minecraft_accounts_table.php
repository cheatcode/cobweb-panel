<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinecraftAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minecraft_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('username');
            $table->integer('owner');
            $table->boolean('confirmed');
            $table->boolean('primary');
            $table->string("verifyCode");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minecraft_accounts');
    }
}
